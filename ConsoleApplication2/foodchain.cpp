#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include "foodchain.h"

/*
플레이어(player)  13명

동물(animal) 포식자 : 사자(1) 악어(2) 독수리(3) 하이에나(4)
피식자 : 수달(5) 사슴(6) 청둥오리(7) 토끼(8) 악어새(9) 쥐(10)  카멜레온(11)  까마귀(12)
특이자 : 뱀(13)
사망후 : (14로 배정)
공격능력판단 attacker : 사자(1) 악어(2) 독수리(3) 하이에나(4) 나머지(5)
위치정보(field) :게임시작전(0) 숲(1) 들(2) 강(3) 하늘(4)  사망(5)

*/

void startgame(void)//처음시작시 나가는 글자
{
	printf("        자세한 게임내용은 네이버에 먹이사슬게임을 검색해주세요.\n\n");
	printf("--------------------------먹 이 사 슬 게 임----------------------------\n");
	printf("동물(animal) \n");
	printf("포식자 : 1.사자 2.악어 3.독수리 4.하이에나\n");
	printf("피식자 : 5.수달 6.사슴 7.청둥오리 8.토끼 \n");
	printf("피식자 : 9.악어새 10.쥐  11.카멜레온  12.까마귀\n");
	printf("특이자 : 13.뱀 \n");
}


int attack(gamer * pointer1, gamer * pointer2)//공격함수
{
	printf("%d.", pointer1->animal);
	animalexchange(pointer1);
	printf("플레이어가 ");
	printf("%d.", pointer2->animal);
	animalexchange(pointer2);
	printf("플레이어를 공격하였습니다.\n");


	if (pointer1->attacker<pointer2->attacker)//플레이어공격승리
	{
		pointer2->field = 5;
		pointer2->life = 0;
		pointer2->victorycondition = 0;
		pointer1->life = (pointer1->life + 1);
		printf("%d.", pointer2->animal);
		animalexchange(pointer2);
		printf("플레이어가 사망하였습니다.\n");
		return 1;
	}
	else if (pointer1->attacker>pointer2->attacker)//플레이어공격패배
	{
		pointer1->field = 5;
		pointer1->life = 0;
		pointer1->victorycondition = 0;
		printf("%d.", pointer1->animal);
		animalexchange(pointer1);
		printf("플레이어가 사망하였습니다.\n");
		return 0;
	}
	else
	{
		printf("아무일도 일어나지 않습니다\n");
	}


	return 1;
}

int Secretattack(gamer * pointer1, gamer * pointer2)//공격함수
{



	if (pointer1->attacker<pointer2->attacker)//공격자승리
	{
		pointer2->field = 5;
		pointer2->life = 0;
		pointer2->victorycondition = 0;
		pointer1->life = (pointer1->life + 1);
		return 1;
	}
	if (pointer1->attacker>pointer2->attacker)//공격자패배
	{
		pointer1->field = 5;
		pointer1->life = 0;
		pointer1->victorycondition = 0;

		return 1;
	}



	return 1;
}

void move1(gamer * pointer)//라운드종료후 랜덤이동
{
	srand(time(NULL));
	if (pointer->life >= 1)//살아있다면 이동
	{
		if (pointer->animal == 8 || pointer->animal == 10 || pointer->animal == 11 || pointer->animal == 13)//토끼 쥐 카멜레온 뱀
		{
			if (pointer->field != pointer->mainfield)
			{
				pointer->field = 1;
			}
			else
			{
				pointer->field = (rand() % 3) + 1;
			}

		}
		if (pointer->animal == 1 || pointer->animal == 4 || pointer->animal == 6)//사자 하이에나 사슴
		{
			if (pointer->field != pointer->mainfield)
			{
				pointer->field = 2;
			}
			else
			{
				pointer->field = (rand() % 3) + 1;
			}
		}
		if (pointer->animal == 2 || pointer->animal == 5)//악어 수달
		{
			if (pointer->field != pointer->mainfield)
			{
				pointer->field = 3;
			}
			else
			{
				pointer->field = (rand() % 3) + 1;
			}
		}
		if (pointer->animal == 3 || pointer->animal == 7 || pointer->animal == 9 || pointer->animal == 12)//독수리 청둥오리 악어새 까마귀 
		{
			if (pointer->field != pointer->mainfield)
			{
				pointer->field = 4;
			}
			else
			{
				pointer->field = (rand() % 4) + 1;
			}
		}

	}
}


int movement(gamer * pointer, int field)//이동보조함수
{
	if (pointer->mainfield != 4 && field == 4)
	{
		return 0;//주서식지가 하늘이 아니면 하늘 선택불가능
	}
	pointer->field = field;
	return 1;
}




void init(gamer * pointer, int data)//초기화함수
{
	srand(time(NULL));
	pointer->animal = data;
	pointer->player = data;
	pointer->field = 0;

	pointer->victorycondition = 1;
	if (data == 1)//사자
	{
		pointer->life = 1;//한번 굶으면 사망
		pointer->attacker = data;
		pointer->attackpoint = 1;//1라운드당 1번 공격

	}
	else if (data == 2 || data == 3)//악어 독수리
	{
		pointer->life = 2;//두번 굶으면 사망
		pointer->attacker = data;
		pointer->attackpoint = 1;//1라운드당 1번 공격

	}
	else if (data == 4)//하이에나
	{
		pointer->life = 3;//두번 굶으면 사망
		pointer->attacker = data;
		pointer->attackpoint = 1;//1라운드당 1번 공격
	}

	else if (data >= 5 && data <= 12)//피식자
	{
		pointer->life = 6;
		pointer->attacker = 5;
		pointer->attackpoint = 0;//공격불가능(고민중 공격은 가능하게 만들까..) 

	}
	else if (data == 13)//뱀
	{
		pointer->life = 6;
		pointer->attacker = 0;//사자보다 강하게 세팅
		pointer->attackpoint = 0;//하지만 공격은 불가능함

	}
	else
	{
		printf("동물 입력값이 잘못되었습니다.");
	}

	if (data == 8 || data == 10 || data == 11 || data == 13)//토끼 쥐 카멜레온 뱀
	{
		pointer->mainfield = 1;//숲
		pointer->field = (rand() % 3) + 1;
	}
	if (data == 1 || data == 4 || data == 6)//사자 하이에나 사슴
	{
		pointer->mainfield = 2;//들
		pointer->field = (rand() % 3) + 1;
	}
	if (data == 2 || data == 5)//악어 수달
	{
		pointer->mainfield = 3;//강
		pointer->field = (rand() % 3) + 1;
	}
	if (data == 3 || data == 7 || data == 9 || data == 12)//독수리 청둥오리 악어새 까마귀 
	{
		pointer->mainfield = 4;//하늘
		pointer->field = (rand() % 4) + 1;
	}
}

void state1(gamer * pointer)//초기상태창함수
{
	puts("-----------------------자신의 상태----------------------------");
	printf("자신은 %d.", pointer->animal);
	animalexchange(pointer);
	printf("입니다\n");
	printf("주서식지는 %d.", pointer->mainfield);
	mainfieldexchange(pointer);
	printf("입니다.\n");
	puts("--------------------------------------------------------------");
}



void state(gamer * pointer)//자신상태창함수
{
	puts("-----------------------자신의 상태----------------------------");
	printf("자신은 %d.", pointer->animal);
	animalexchange(pointer);
	printf("입니다\n");
	printf("주서식지는 %d.", pointer->mainfield);
	mainfieldexchange(pointer);
	printf("입니다.\n");
	printf("현재 위치는 %d.", pointer->field);
	fieldexchange(pointer);
	printf("입니다.\n");
	puts("--------------------------------------------------------------");
}


int location(gamer * pointer, int data)//현재 동물 위치
{
	if (pointer->field == data)
	{
		printf("%d.", pointer->animal);
		animalexchange(pointer);
		return 1;
	}
	return 0;
}

void animalexchange(gamer * pointer)//동물숫자를 문자로 변경
{
	if (pointer->animal == 1)
		printf("사자 ");
	if (pointer->animal == 2)
		printf("악어 ");
	if (pointer->animal == 3)
		printf("독수리 ");
	if (pointer->animal == 4)
		printf("하이에나 ");
	if (pointer->animal == 5)
		printf("수달 ");
	if (pointer->animal == 6)
		printf("사슴 ");
	if (pointer->animal == 7)
		printf("청둥오리 ");
	if (pointer->animal == 8)
		printf("토끼 ");
	if (pointer->animal == 9)
		printf("악어새 ");
	if (pointer->animal == 10)
		printf("쥐 ");
	if (pointer->animal == 11)
		printf("카멜레온 ");
	if (pointer->animal == 12)
		printf("까마귀 ");
	if (pointer->animal == 13)
		printf("뱀 ");
}

void fieldexchange(gamer * pointer)//장소숫자를 문자로 변경
{
	if (pointer->field == 1)
		printf("숲 ");
	if (pointer->field == 2)
		printf("들 ");
	if (pointer->field == 3)
		printf("강 ");
	if (pointer->field == 4)
		printf("하늘 ");
	if (pointer->field == 5)
		printf("사망 ");
}

void mainfieldexchange(gamer * pointer)//메인장소숫자를 문자로 변경
{
	if (pointer->mainfield == 1)
		printf("숲 ");
	if (pointer->mainfield == 2)
		printf("들 ");
	if (pointer->mainfield == 3)
		printf("강 ");
	if (pointer->mainfield == 4)
		printf("하늘 ");

}

void nowdie(gamer * pointer)//현재 라운드의 생존동물 검색
{
	if (pointer->life <= 0)
	{
		printf("%d.", pointer->animal);
		animalexchange(pointer);
	}
}

int attacklimiter(gamer * pointer)//라운드당 1번만 공격가능
{
	if (pointer->attackpoint == 1)
		return 1;

	return 0;
}

void AttackPointdown(gamer * pointer)//공격한번하면 포인트다운
{
	pointer->attackpoint = 0;
}

void AttackPointup(gamer * pointer)//라운드지나면서 공격포인트회복
{
	if (pointer->animal >= 1 && pointer->animal <= 4)
	{
		pointer->attackpoint = 1;
	}
}

void victory(gamer * pointer)//승리출력문구
{
	if (pointer->victorycondition == 1)
	{
		printf("%d.", pointer->animal);
		animalexchange(pointer);
		printf(" 플레이어는 승리하셨습니다.\n");
	}

}

void lifedown(gamer * pointer)//라운드종료시 라이프1개씩 감소
{
	pointer->life = (pointer->life - 1);
}

void lifedie(gamer * pointer)//라이프가0인동물은 사망
{
	if (pointer->life == 0)
	{
		pointer->field = 5;
		pointer->victorycondition = 0;
	}

}

int palyerdie(gamer * pointer)//플레이어 사망판단
{
	if (pointer->life == 0)
		return 0;

	return 1;
}

int cbirdcondition(gamer * pointer)//악어새 승리조건판단
{
	if (pointer->victorycondition == 1)
	{
		return 1;
	}



	return 0;
}

int mousecondition(gamer * pointer)//쥐 승리조건판단
{

	if (pointer->victorycondition == 1)
	{
		return 1;
	}

	return 0;
}




void winnerchance(gamer * pointer)//승자로바꿈
{
	pointer->victorycondition = 1;
}

void losschance(gamer * pointer)//패자로바꿈
{
	pointer->victorycondition = 0;
}

int crowdecision(gamer * pointer)//까마귀의 승리조건판단
{
	if (pointer->victorycondition == 1)
	{
		return 1;
	}
	return 0;
}

int hyenawincondition(gamer * pointer)//하이에나승리조건판단
{
	if (pointer->victorycondition == 0)
	{
		return 1;
	}
	return 0;
}

int hyenalosscondition(gamer * pointer)//하이에나 패배조건판단
{
	if (pointer->victorycondition == 1)
	{
		return 1;
	}
	return 0;
}

int snakecondition(gamer * pointer)//뱀 승리조건판단
{
	if (pointer->life >= 1)
	{
		return 1;
	}
	return 0;
}

int roundattacker(gamer * pointer1, gamer * pointer2)//라운드마다 포식자는 한번씩 공격을 한다.
{

	if (pointer1->field == pointer2->field)
	{
		Secretattack(pointer1, pointer2);
		return 1;
	}

	return 0;

}

int playergameout(gamer * pointer)
{
	if (pointer->victorycondition == 0)
	{
		return 1;
	}

	return 0;
}

int playermainfiledlimit(gamer * pointer, int num)//플레이어 위치가 주거주지가 아니면 주거주지로 이동
{
	if (pointer->mainfield == pointer->field)
	{
		return num;
	}

	if (pointer->mainfield != num)
	{
		printf("주거주지로 이동해야합니다.  \n\n");
		return -1;
	}

	return num;
}

int roundattackcondition(gamer * pointer)//생존한 육식동물만 라운드에 공격
{
	if (pointer->life == 0)
	{
		return 0;
	}

	return 1;

}


int animalunion(gamer * pointer1, gamer * pointer2, gamer * pointer3, gamer * pointer4)//4인조 특수능력성공판단
{
	int a = pointer1->field * 1000;//천의자리
	int	b = pointer2->field * 100;//백의자리
	int	c = pointer3->field * 10;//십의자리
	int	d = pointer4->field * 1;//일의자리
	int e = 1111, f;

	if (pointer1->field == 5)//사망시 0으로 변경
	{
		a = 0;
		e = e - 1000;
	}
	if (pointer2->field == 5)
	{
		b = 0;
		e = -100;
	}

	if (pointer2->field == 5)
	{
		c = 0;
		e = e - 10;
	}

	if (pointer2->field == 5)
	{
		d = 0;
		e = e - 1;
	}

	f = a + b + c + d;
	if (f%e == 0)
	{
		return 1;//특수능력발동
	}

	return 0;//아무일도없음
}