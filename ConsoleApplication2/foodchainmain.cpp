#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include"foodchain.h"

/*
플레이어 구조체정의구현
메인함수변수지정구현
게임시작문구 구현
각동물들 초기화구현
플레이어 동물 랜덤선택구현
초기상태창함수구현
플레이어 지역이동구현
각 라운드마다 동물 랜덤이동구현(비행동물이아니면 하늘은 선택불가)
동물들은 현재위치가 주서식지가 아니면 다음라운드에는 주서식지도 이동하게 만듬
플레이어가 육식자일때 라운드별 1번만 공격가능하게 만듬
메인메뉴 1.현재상태검색 2.현재동물검색 3.공격 4.라운드종료 구현
공격시 현재지역에 있는동물들만 공격가능 구현
승리동물출력문 구현
라이프가 부족한 동물은 사망 구현
플레이어가 라이프가 부족하면 게임종료
뱀은 공격할수없음 구현
뱀은 공격받을시 상대방 사망 구현
승리조건중 악어새-생존or악어승리 쥐-생존or사자승리 까마귀-생존or승리자예측맞을시 하이에나-사자생존(패)or사자사망(승) 뱀-동물이9명이상사망시 구현
플레이어 위치가 주서식지가 아닐때 다음라운드에 주서식지로 이동
라운드마다 육식동물들의 랜덤공격
라운드후랜덤공격에서는 발동: 4인조 특수능력 수달 청동오리 토끼 사슴 수달이 한장소에 있으면 죽지않는다.


미구현
카멜레온 특수능력

*/

int main()
{
	int select;//플레이어에게 선택된케릭터
	int sel = 0;//더미같은 역할 case2 for문에사용함
	int move = 0;//이동변수
	int round = 1;//라운드변수
	int attacknum = 0;//공격관련변수
	int die = -1;//사망관련변수
	int menunum = -1;//메뉴변수
	int dummy1 = 0, dummy2 = 0, dummy3 = 0, dummy4 = -1, dummy5 = 0;//for문이나 잡다한곳에 사용
	int frame1 = 0, frame2 = -1;//사용금지
	int animal[13];//판단변수
	int attacklimite;//공격가능한지 판단함수
	int crow = -1;//까마귀승리자판단변수
	int snakenum = 0;
	int skillfour = -1;//4인조특수능력변수(1일때 발동)
	int playdiedecision = -1;//플레이어가 사망했는지 판단 변수 만약 1이되면 게임종료
	gamer palyer[13];//플레이어 구조체


	for (dummy1 = 0; dummy1 <= 12; dummy1++)//초기화진행
	{
		init(&palyer[dummy1], dummy1 + 1);
	}

	srand(time(NULL));//램덤케릭터선택하기위한선함수
	startgame();//시작문구

				//아래주석은 자신이  직접 케릭터 선택하여 게임플레이
	printf("자신의 케릭터를 선택해주세요 : ");
	scanf("%d", &select);
	printf("\n");

	if (select >= 14 || select <= 0)//1~13까지만 선택되게 제한을둠(개선요망)
	{
		select = -1;
		for (; select >= 14 || select <= 0;)
		{
			printf("잘못입력되었습니다\n");
			printf("자신의 케릭터를 선택해주세요 : ");
			scanf("%d", &select);
			printf("\n");
		}
	}

	//select=(rand()%13+1);//자동 케릭터선택

	state1(&palyer[select - 1]);//자신상태창


	if (select == 12)//까마귀 승리조건 관련
	{
		printf("\n");
		printf("승리자를 판단해주세요 : ");
		scanf("%d", &crow);
		printf("\n");
		while (crow >= 14 || crow <= 0)
		{
			printf("1~13까지만 선택이 가능합니다.\n");
			printf("승리자를 판단해주세요 : ");
			scanf("%d", &crow);
			printf("\n");
		}

	}

	while (dummy5 == 0)
	{
		printf("위치정보(field) : 1.숲 2.들 3.강 4.하늘\n");
		printf("이동할 장소를 선택해주세요 : ");
		scanf("%d", &move);

		if (move >= 5 || move <= 0)//1~4만 선택가능하게 변경
		{
			move = -1;
			for (; move >= 5 || move <= 0;)
			{
				printf("잘못입력되었습니다 \n");
				printf("위치정보(field) : 1.숲 2.들 3.강 4.하늘\n");
				printf("이동할 장소를 선택해주세요 : ");
				scanf("%d", &move);
			}
		}

		dummy5 = movement(&palyer[select - 1], move);//이동함수 (갈수없는장소 미구현)
		if (dummy5 == 0)
		{
			printf("하늘로 이동할수없는 동물입니다.\n");
		}
	}
	if (playdiedecision != 1)//사망시 메뉴 제한
	{
		printf("%d.", move);
		fieldexchange(&palyer[select - 1]);
		printf("으로 이동하였습니다.\n\n");
	}

	for (round = 1; round <= 4; )
	{
		printf("============================%d 라 운 드 시 작=============================\n", round);
		if (playdiedecision != 1)//사망시 메뉴 제한
		{
			while (menunum != 4)
			{
				printf("1.현재상태검색 2.현재동물검색 3.공격 4.라운드종료 \n");
				printf("번호입력: ");
				scanf("%d", &menunum);

				skillfour = animalunion(&palyer[4], &palyer[5], &palyer[6], &palyer[7]);//4인조 특수능력발동판단(1)반환시 성공

				switch (menunum)
				{
				case 1:
					state(&palyer[select - 1]);
					break;
				case 2:
					printf("현재위치에 있는 동물 : ");

					for (sel = 0; sel <= 12; sel++)
					{
						animal[sel] = location(&palyer[sel], move);
					}
					printf("\n");
					printf("\n");



					break;
				case 3:

					attacklimite = attacklimiter(&palyer[select - 1]);

					if (attacklimite == 0)
					{
						printf("육식자는 라운드당 1번만 공격가능 피식자는 공격할수없습니다.\n\n");
						break;
					}



					printf("현재위치에 있는 동물 : ");//공격버튼누르면 동물위치보여줌

					for (sel = 0; sel <= 12; sel++)
					{
						animal[sel] = location(&palyer[sel], move);//같은위치에 있는동물들은 animal[sel]에서 1로 표시된다.
					}
					printf("\n\n");

					printf("어떤동물을 공격하시겠습니까? :");
					scanf("%d", &attacknum);
					printf("\n");
					while (skillfour == 1 && (attacknum == 5 || attacknum == 6 || attacknum == 7 || attacknum == 8))
					{
						printf("5.수달 6.사슴 7.청동오리 8.토끼의 특수능력발동중입니다.! \n\n");
						printf("어떤동물을 공격하시겠습니까? :");
						scanf("%d", &attacknum);
						printf("\n");
					}

					if (animal[attacknum - 1] != 1)//같은위치에 있는 동물들만 공격가능
					{

						while (animal[attacknum - 1] != 1)
						{
							printf("같은위치에 없는 동물입니다. 다시 선택해주세요. \n\n");
							printf("어떤동물을 공격하시겠습니까? :");
							scanf("%d", &attacknum);
							printf("\n");
						}
					}


					die = attack(&palyer[select - 1], &palyer[attacknum - 1]);//공격함수

					if (die == 0)//강한동물에게 공격해서 사망
					{
						printf("====당신은 사망하였습니다!!==== \n");
						printf("\n");
						return 0;
					}

					AttackPointdown(&palyer[select - 1]);

					break;

				case 4:
					printf("============================%d 라 운 드 종 료=============================\n\n", round);

					break;
				default:
					printf("잘못된 숫자를 입력하셨습니다 \n");
					printf("\n");
					break;
				}

			}
		}
		if (round == 4)
			break;

		//4인조(5678)
		skillfour = animalunion(&palyer[4], &palyer[5], &palyer[6], &palyer[7]);//4인조 특수능력발동판단(1)반환시 성공
		if (skillfour == 1)
		{
			printf("\n수달 토끼 사슴 청동오리 특수능력발동!\n\n");

		}


		for (dummy1 = 0; dummy1<4; dummy1++)//생존한 육식동물은 라운드 종료후 그지역의 동물을 랜덤으로 공격한다.
		{
			if (roundattackcondition(&palyer[dummy1]) == 1)
			{
				if (dummy1 != select - 1)//플레이어는 랜덤공격에서 제외
				{
					for (dummy5 = 0; dummy5<200; dummy5++)//200번 목표물을 찾아본다..
					{

						dummy3 = rand() % 12;//공격당하는 동물을 랜덤으로 숫자를 부여한다.(뱀은 공격하지 않는다.)
						if (skillfour != 1 && (dummy3 != 4 || dummy3 != 5 || dummy3 != 6 || dummy3 != 7))//4인조 특수능력이 발동되지않으면 실행한다.
						{
							if (dummy1 != dummy3)//서로 다른 동물이라면 같은지역인지 확인후 공격한다.
							{
								dummy2 = roundattacker(&palyer[dummy1], &palyer[dummy3]);//공격해보는데 같은위치가 아니면 공격안함
							}

							if (dummy2 == 1)//만약 공격자가 피식자를 찾는다면 반복문종료
							{
								frame2 = dummy1;//만약 플레이어가 사망했다면 누구에게 죽었는지 확인
								dummy5 = 200;//반복문 종료
							}
						}
					}//300번 찾아봤는데 없다면 그라운드에는 굶는다.
				}
			}
		}


		for (dummy3 = 0; dummy3 <= 12; dummy3++)//라운드종료 후 랜덤이동
		{
			if (dummy3 != select - 1)
			{
				move1(&palyer[dummy3]);
			}
		}

		skillfour = -1;
		menunum = 0;

		for (dummy3 = 0; dummy3 <= 12; dummy3++)//라운드종료 후 라이프1씩감소
		{
			lifedown(&palyer[dummy3]);
		}

		for (dummy3 = 0; dummy3 <= 12; dummy3++)//라운드종료 후 라이프0인동물 사망처리
		{
			lifedie(&palyer[dummy3]);


		}

		die = palyerdie(&palyer[select - 1]);//공격함수

		if (die == 0)//굶어서 사망
		{
			printf("=================게임 오버================ \n");
			printf("당신은 굶어죽었습니다\n\n");
			return 0;
		}

		printf("현재까지 사망한 동물\n");
		for (dummy3 = 0; dummy3 <= 12; dummy3++)//라운드종료 후 현재 사망중인 동물들 출력
		{
			nowdie(&palyer[dummy3]);
		}
		printf("\n\n");//한칸엔터



		if (playdiedecision != 1)//사망시 메뉴 제한
		{
			while (frame1 == 0)
			{
				printf("위치정보(field) : 1.숲 2.들 3.강 4.하늘\n");
				printf("이동할 장소를 선택해주세요 : ");
				scanf("%d", &move);
				move = playermainfiledlimit(&palyer[select - 1], move);//주거주지가 아니면 주거주지로 이동하자

				if (move >= 5 || move <= 0)//1~4만 선택가능하게 변경
				{
					move = -1;
					for (; move >= 5 || move <= 0;)
					{
						printf("위치정보(field) : 1.숲 2.들 3.강 4.하늘\n");
						printf("이동할 장소를 선택해주세요 : ");
						scanf("%d", &move);
						move = playermainfiledlimit(&palyer[select - 1], move);//주거주지가 아니면 주거주지로 이동하자
					}
				}

				frame1 = movement(&palyer[select - 1], move);//이동함수 (갈수없는장소 미구현)
				if (frame1 == 0)
				{
					printf("하늘로 이동할수없는 동물입니다.\n\n");
				}
			}
			frame1 = 0;//다시 0으로 바꿔 위쪽 while을 유지하자
		}


		playdiedecision = playergameout(&palyer[select - 1]);//플레이어 사망판단
		if (playdiedecision == 1)//사망시 출력문
		{
			printf("%d.", frame2 + 1);
			animalexchange(&palyer[frame2]);
			printf("에게 공격당하여 사망하였습니다. %d라운드에 게임에서 제외됩니다.\n\n", round);
			printf("=============사 망==============");
		}

		if (playdiedecision != 1)//사망시 메뉴 제한
		{

			movement(&palyer[select - 1], move);

			printf("%d.", move);
			fieldexchange(&palyer[select - 1]);
			printf("으로 이동하였습니다.\n\n");
		}



		for (dummy3 = 0; dummy3 <= 3; dummy3++)//라운드종료 후 공격포인트1
		{
			AttackPointup(&palyer[dummy3]);
		}



		round++;

	}


	dummy2 = hyenawincondition(&palyer[0]);//하이에나 승리조건 판단
	if (dummy2 == 1)
	{
		winnerchance(&palyer[3]);
	}

	dummy2 = hyenalosscondition(&palyer[0]);//하이에나 패배조건 판단
	if (dummy2 == 1)
	{
		losschance(&palyer[3]);
	}

	if (select == 12)
	{
		dummy3 = crowdecision(&palyer[crow - 1]);//까마귀 승리조건 판단
		if (dummy3 == 0)
		{
			losschance(&palyer[11]);
		}
	}




	dummy4 = cbirdcondition(&palyer[1]);//악어새 승리조건
	if (dummy4 == 1)
	{
		winnerchance(&palyer[8]);
	}
	if (dummy4 != 1)//악어새 패배조건
	{
		losschance(&palyer[8]);
	}

	dummy5 = mousecondition(&palyer[0]);//쥐 승리조건
	if (dummy5 == 1)
	{
		winnerchance(&palyer[9]);
	}
	if (dummy5 != 1)//쥐 패배조건
	{
		losschance(&palyer[9]);
	}

	for (dummy4 = 0; dummy4 <= 12; dummy4++)//뱀 승리조건
	{
		snakenum = snakenum + snakecondition(&palyer[dummy4]);
	}
	if (snakenum>4)
	{
		losschance(&palyer[12]);
	}


	for (dummy1 = 0; dummy1 <= 12; dummy1++)//승리자출력문
	{
		victory(&palyer[dummy1]);
	}

	return 0;


}