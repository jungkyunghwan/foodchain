#ifndef __FOODCHAIN
#define __FOODCHAIN

/*
플레이어(player)  13명

동물(animal) 포식자 : 사자(1) 악어(2) 독수리(3) 하이에나(4)
피식자 : 수달(5) 사슴(6) 청둥오리(7) 토끼(8) 악어새(9) 쥐(10)  카멜레온(11)  까마귀(12)
특이자 : 뱀(13)
사망후 : (14로 배정)
공격능력판단 attacker : 사자(1) 악어(2) 독수리(3) 하이에나(4) 나머지(5)
위치정보(field) : 숲(1) 들(2) 강(3) 하늘(4)  사망후 : (5번으로 배정)

*/

typedef struct _gamestruct
{
	int player;//1~13
	int animal;//1~13
	int attacker;//1사자 2악어 3독수리 4하이에나 5나머지
	int attackpoint;//라운드별 1번만 공격할수있게 제한
	int field;//현재 거주지(5번은 사망시 이동)
	int mainfield;//주 거주지
	int life;//생존여부 (사망0 생존1) 이함수가 있어야 죽은 동물들이 다음 라운드에 부활하지않음
	int victorycondition;//(패배0 승리1) 초기에는 모두 1로 시작함
}gamer;

void startgame(void);//처음시작시 나가는 글자
int attack(gamer * pointer1, gamer * pointer2);//공격성공시 라이프1개 획득
int Secretattack(gamer * pointer1, gamer * pointer2);//메세지가 없는 공격
void move1(gamer * pointer);//라운드 종료시 랜덤으로 장소이동
int movement(gamer * pointer, int field);//이동보조함수
void init(gamer * pointer, int data);//초기화함수
void state1(gamer * pointer);//초기상태창함수
void state(gamer * pointer);//상태창함수
int location(gamer * pointer, int data);//현재 동물 위치
void animalexchange(gamer * pointer);//동물숫자를 문자로 변경
void fieldexchange(gamer * pointer);//장소숫자를 문자로 변경
void mainfieldexchange(gamer * pointer);//메인장소숫자를 문자로 변경
void nowdie(gamer * pointer);//현재 라운드의 생존동물 검색(현재미사용)
int attacklimiter(gamer * pointer);//라운드당 1번만 공격가능
void AttackPointdown(gamer * pointer);//공격한번하면 포인트다운
void AttackPointup(gamer * pointer);//라운드지나면서 공격포인트회복
void victory(gamer * pointer);//승리 출력 문구
void lifedown(gamer * pointer);//라운드종료시 라이프1개씩 감소
void lifedie(gamer * pointer);//라이프가0인동물은 사망
int palyerdie(gamer * pointer);//플레이어 사망판단
int cbirdcondition(gamer * pointer);//악어새의 승리조건판단
int mousecondition(gamer * pointer);//쥐 승리조건 판단
int crowdecision(gamer * pointer);//까마귀의 승리조건판단
int hyenawincondition(gamer * pointer);//하이에나 승리조건판단
int hyenalosscondition(gamer * pointer);//하이에나 패배조건판단
void winnerchance(gamer * pointer);//승자로바꿈
void losschance(gamer * pointer);//패자로 바꿈
int snakecondition(gamer * pointer);//뱀 승리조건판단
int roundattacker(gamer * pointer1, gamer * pointer2);//라운드마다 포식자는 한번씩 공격을 한다.
int playergameout(gamer * pointer);//플레이어 사망판단
int playermainfiledlimit(gamer * pointer, int num);//플레이어 위치가 주거주지가 아니면 주거주지로 이동
int roundattackcondition(gamer * pointer);//생존한 육식동물만 라운드에 공격
int animalunion(gamer * pointer1, gamer * pointer2, gamer * pointer3, gamer * pointer4);//4인조 특수능력성공판단


#endif